import org.apache.log4j.Logger;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static ExecutorService executorService = Executors.newSingleThreadExecutor();
    private static final Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Starting client");
        createIfDoNotExistFolderForLoad();
        executorService.execute(new Client());
        logger.info("Start thread for interaction with server");
        executorService.shutdown();
    }

    public static void createIfDoNotExistFolderForLoad() {
        File folder = new File("files");
        if (!folder.exists()) {
            folder.mkdir();
            logger.info("Connection folder for load");
        }
    }

}
