import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client implements Runnable {
    private static Socket socket;
    private static final Logger logger = Logger.getLogger(Main.class);
    private static final int PORT_SERVER = 3300;
    private static final String IP_SERVER = "localhost";
    private static final String EXIT_MASSAGE = "0";
    private static final String GET_FILE_NAME_LIST_MASSAGE = "1";
    private static final String GET_FILE_MASSAGE = "2";

    public Client() {
        try {
            socket = new Socket(IP_SERVER, PORT_SERVER);
            logger.info("Client connected to socket");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public void run() {
        logger.info("Thread for interaction with server is shutdown");
        printCommands();
        try (DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
             DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
             InputStream inputStream = socket.getInputStream()) {
            logger.info("Client DataOutputStream, DataInputStream and InputStream initialized");
            String entry = "";
            dataOutputStream.writeUTF("start");
            ObjectInputStream objectInputStream = null;
            while (!entry.equals(EXIT_MASSAGE)) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
                entry = bufferedReader.readLine();
                logger.info("Enter massage from console '" + entry + "'");
                switch (entry) {
                    case (EXIT_MASSAGE):
                        dataOutputStream.writeUTF("stop");
                        dataOutputStream.flush();
                        logger.info("Sent 'stop' massage");
                        System.out.println(dataInputStream.readUTF());
                        logger.info("Stop client");
                        objectInputStream.close();
                        logger.info("Closing objectInputStream");
                        break;
                    case (GET_FILE_NAME_LIST_MASSAGE):
                        dataOutputStream.writeUTF("get list");
                        dataOutputStream.flush();
                        logger.info("Sent 'get list' massage");
                        objectInputStream = new ObjectInputStream(socket.getInputStream());
                        printStringArray((String[]) objectInputStream.readObject());
                        break;
                    case (GET_FILE_MASSAGE):
                        System.out.println("Enter file name: ");
                        entry = bufferedReader.readLine();
                        logger.info("Enter file for loading");
                        dataOutputStream.writeUTF("get file:" + entry);
                        dataOutputStream.flush();
                        logger.info("Sent 'get file' massage");
                        if (dataInputStream.readBoolean()) {
                            saveFile(dataInputStream, inputStream, entry);
                            logger.info("Load file: " + entry);
                        } else {
                            System.out.println("File don't exist");
                            logger.info("Get flag that file '" + entry + "' doesn't exist");
                        }
                        break;
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    private void printStringArray(String[] strings) {
        List list = new ArrayList<>(Arrays.asList(strings));
        list.forEach(string -> System.out.println(string));
        logger.info("Print list of files");
    }

    private void saveFile(DataInputStream dataInputStream, InputStream inputStream, String fileName) {
        try (FileOutputStream fileOutputStream = new FileOutputStream("files/" + fileName)) {
            long byteArraySize = dataInputStream.readLong();
            logger.info("Getting size of file");
            int readBytes;
            byte[] buffer = new byte[1024];
            while (byteArraySize > 0) {
                readBytes = inputStream.read(buffer, 0, buffer.length);
                fileOutputStream.write(buffer, 0, readBytes);
                byteArraySize -= readBytes;
            }
            fileOutputStream.flush();
            logger.info("Loading of file");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void printCommands() {
        System.out.println("Enter number:");
        System.out.println("  0 - for exit");
        System.out.println("  1 - for getting list of names of files");
        System.out.println("  2 - for load file");
        logger.info("Print list of commands for interaction with user");
    }

}
